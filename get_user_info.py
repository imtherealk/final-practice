import csv

def get_user_info():
    name, email, phone = input().split(",")
    with open("users.csv", "w") as f:
        csv.writer(f).writerow(
            [name, email, phone]
        )

get_user_info()
